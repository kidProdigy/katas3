const sampleArray = [469, 755, 244, 245, 758, 450, 302, 20, 712, 71, 456, 21, 398, 339, 882, 848, 179, 535, 940, 472];

let kata1 = function () {
    let div=document.getElementById("kata1")
    let output=""
    for (let x=1; x<=25; x+=1){
        output = output + x + ", "
    }
    printText(output, div)
}

function kata2 () {
    let div=document.getElementById("kata2")
    let output=""
    for (let x=25; x>0; x-=1){
        output = output + x + ", "
    }
    printText(output, div)
}

function kata3 () {
    let div=document.getElementById("kata3")
    let output=""
    for (let x=(-1); x>=(-25); x-=1){
        output = output + x + ", "
    }
    printText(output, div)
}

function kata4 () {
    let div=document.getElementById("kata4")
    let output=""
    for (let x=(-25); x<=(-1); x+=1){
        output = output + x + ", "
    }
    printText(output, div)
}

function kata5 () {
    let div=document.getElementById("kata5")
    let output=""
    for (let x=25; x>=(-25); x-=1){
        output = output + x + ", "
    }
    printText(output, div)
}

function kata6 () {
    let div=document.getElementById("kata6")
    let output=""
    for (let x=1; x<=100; x+=1){
        if (x%3==0){
            output = output + x + ", "
        }
    }
    printText(output, div)
}

function kata7 () {
    let div=document.getElementById("kata7")
    let output=""
    for (let x=1; x<=100; x+=1){
        if (x%7==0){
            output = output + x + ", "
        }
    }
    printText(output, div)
}

function kata8 () {
    let div=document.getElementById("kata8")
    let output=""
    for (let x=1; x<=100; x+=1){
        if (x%3==0 || x%7==0){
            output = output + x + ", "
        }
    }
    printText(output, div)
}

function kata9 () {
    let div=document.getElementById("kata9")
    let output=""
    for (let x=1; x<=100; x+=1){
        if (x%2==1 && x%5==0){
            output = output + x + ", "
        }
    }
    printText(output, div)
}

function kata10 () {
    let div=document.getElementById("kata10")
    let output=sampleArray
    printText(output, div)
}

function kata11 (x) {
    let div=document.getElementById("kata11")
    let output=""
    sampleArray[x];
    for (let x = 0; x < sampleArray.length; x++) {
        if(sampleArray[x] % 2 == 0) { 
            output = output + sampleArray[x] + ", ";
        }
    }
    printText(output, div)
}

function kata12 (x) {
    let div=document.getElementById("kata12")
    let output=""
    sampleArray[x];
    for (let x = 0; x < sampleArray.length; x++) {
        if(sampleArray[x] % 2 == 1) { 
            output = output + sampleArray[x] + ", ";
        }
    }
    printText(output, div)
}

function kata13 (x) {
    let div=document.getElementById("kata13")
    let output=""
    sampleArray[x];
    for (let x = 0; x < sampleArray.length; x++) {
        if(sampleArray[x] % 8 == 0) { 
            output = output + sampleArray[x] + ", ";
        }
    }
    printText(output, div)
}

function kata14 (x) {
    let div=document.getElementById("kata14")
    let output=""
    sampleArray[x];
    let squareA=0
    for (let x = 0; x < sampleArray.length; x++) {
        squareA=sampleArray[x] * sampleArray[x] 
            output = output + squareA + ", ";
        }
    
    printText(output, div)
}

function kata15 () {
    let div=document.getElementById("kata15")
    let output=""
    let sum=0
    for (let x=1; x<=20; x++){
        sum= x+sum
    }
    output = output + sum;
    printText(output, div)
}

function kata16 (x) {
    let div=document.getElementById("kata16")
    let output=""
    sampleArray[x];
    let sum=0
    for (let x = 0; x < sampleArray.length; x++) {
        sum=sampleArray[x]+sum 
            ;
        }
    output = output + sum;
    printText(output, div)
}

function kata17 (x) {
    let div=document.getElementById("kata17")
    let output=""
    sampleArray[x];
    let smallest=sampleArray[1]
    for (let x = 0; x < sampleArray.length; x++) {
        if(sampleArray[x]<smallest) { 
            smallest = sampleArray[x];
        }
    }
    output = output + smallest;
    printText(output, div)
}

function kata18 (x) {
    let div=document.getElementById("kata18")
    let output=""
    sampleArray[x];
    let biggest=sampleArray[1]
    for (let x = 0; x < sampleArray.length; x++) {
        if(sampleArray[x]>biggest) { 
            biggest = sampleArray[x];
        }
    }
    output = output + biggest;
    printText(output, div)
}

kata1()
kata2()
kata3()
kata4()
kata5()
kata6()
kata7()
kata8()
kata9()
kata10()
kata11()
kata12()
kata13()
kata14()
kata15()
kata16()
kata17()
kata18()

function printText(output, div){
    let newText=document.createTextNode(output)
    let newPara=document.createElement("p")
    newPara.appendChild(newText)
    div.appendChild(newPara)
}
